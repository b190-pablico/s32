// importing the "http" module
const http = require("http");

// storing the 4000 in a variable called port
const port = 4000;

// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {

	if (request.url === "/items" && request.method === "GET"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Data retrieved from the database.");
	}

	if (request.url === "/items" && request.method === "POST"){
		response.writeHead(200, { "Content-Type": "text/plain"});
		response.end("Data to be sent to the database");
	}

});
/*
	GET method is one of the HTTPS methods that we will be using from this point.
		GET method means that we will be retrieving or reading information

*/

/*
	POSTMAN
*/

// using server and port variables
server.listen(port);

console.log(`Server now running at port: ${port}`);